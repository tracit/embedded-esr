
# Environmental sound recognition on embedded systems: from FPGAs to TPUs
## Description
A performance comparison of Convolutional Neural Network inference, for classifying environmental sounds, on embedded systems. This project compares inference latency and accuracy on FPGAs versus other embedded platforms such as the general purpose Raspberry pi and a dedicated Tensor Processing Unit. Two different CNNs are evaluated: **CNN1D** uses 1-dimensional features and convolutions, while **CNN2D** makes use of spectrogram images and 2D convolutions. More information, together with the methodology and results can be found in [**our paper**](https://www.mdpi.com/2079-9292/10/21/2622) (for citation see below).

## Platforms
FPGA: Vitis AI Deep Processing Unit & [hls4ml]([github.com/fastmachinelearning/hls4ml](url))

General-purpose embedded platform: Raspberry Pi 4

TPU-based platforms: USB Coral & Coral DevBoard ([https://coral.ai/](url))

## Project overview

### Training + performance experiments
The folder 'Training + performance experiments' contains the source code for training the two CNNs in TensorFlow, together with some platform-independent experiments concerning accuracy. It contains also the code for creating an hls4ml project, including training, model compression and conversion to an hls4ml model.

Training for CNN1D is done in [Google Colab](https://colab.research.google.com/notebooks/welcome.ipynb), an environment that allows training in the cloud on GPUs & TPUs. Training for CNN2D and conversion to hls4ml models is done locally.

### Embedded experiments
The folder 'Embedded experiments' contains all the source code for the experiments with Raspberry Pi, USB Coral and Coral DevBoard. The flow for each platform is shown in the figure below, taken from our paper.

<img
src="Figures/tool_flow_embedded_platforms.PNG"
raw=true
alt="Tool flows embedded platforms"
style="margin-right: 10px;"
/>


## Cite

~~~bibtex
@Article{electronics10212622,
AUTHOR = {Vandendriessche, Jurgen and Wouters, Nick and da Silva, Bruno and Lamrini, Mimoun and Chkouri, Mohamed Yassin and Touhafi, Abdellah},
TITLE = {Environmental Sound Recognition on Embedded Systems: From FPGAs to TPUs},
JOURNAL = {Electronics},
VOLUME = {10},
YEAR = {2021},
NUMBER = {21},
ARTICLE-NUMBER = {2622},
URL = {https://www.mdpi.com/2079-9292/10/21/2622},
ISSN = {2079-9292},
ABSTRACT = {In recent years, Environmental Sound Recognition (ESR) has become a relevant capability for urban monitoring applications. The techniques for automated sound recognition often rely on machine learning approaches, which have increased in complexity in order to achieve higher accuracy. Nonetheless, such machine learning techniques often have to be deployed on resource and power-constrained embedded devices, which has become a challenge with the adoption of deep learning approaches based on Convolutional Neural Networks (CNNs). Field-Programmable Gate Arrays (FPGAs) are power efficient and highly suitable for computationally intensive algorithms like CNNs. By fully exploiting their parallel nature, they have the potential to accelerate the inference time as compared to other embedded devices. Similarly, dedicated architectures to accelerate Artificial Intelligence (AI) such as Tensor Processing Units (TPUs) promise to deliver high accuracy while achieving high performance. In this work, we evaluate existing tool flows to deploy CNN models on FPGAs as well as on TPU platforms. We propose and adjust several CNN-based sound classifiers to be embedded on such hardware accelerators. The results demonstrate the maturity of the existing tools and how FPGAs can be exploited to outperform TPUs.},
DOI = {10.3390/electronics10212622}
}
~~~


## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Project status: in progress
...

