#!/bin/bash

# Activate the correct conda environment
conda activate vitis-ai-tensorflow2

# Validation/Test fold
export valFold=9
export testFold=10

# network parameters
export INPUT_HEIGHT=128 # = number of mels in spectrogram
export INPUT_WIDTH=128 
export INPUT_CHANNEL=1
export NET_NAME=SBCNN

# folders
export BUILD=./build/fold${testFold}
export TARGET=${BUILD}/target
export LOG=${BUILD}/logs
export TB_LOG=${BUILD}/tb_logs
export KERAS=${BUILD}/keras_model
export COMPILE=${BUILD}/compile/
export QUANT=${BUILD}/quantize
export TFCKPT_DIR=${BUILD}/tf_chkpt

# make the log folder
mkdir -p ${LOG}

# logs & results files
export TRAIN_LOG=train.log
export TEST_LOG=test.log
export QUANT_LOG=quant.log
export COMP_LOG=compile.log

# Keras checkpoint file
export K_MODEL=k_model.h5

# quantized model file
export Q_MODEL=Q_model.h5

# export all images?
export TEST_ALL_IMAGES=0

# target board
export BOARD=ZCU104

# NOTE: this directory only exists INSIDE the docker container, it is not present on the system
export ARCH=/opt/vitis_ai/compiler/arch/DPUCZDX8G/${BOARD}/arch.json

# DPU mode - best performance with DPU_MODE = normal
# export DPU_MODE=debug
export DPU_MODE=normal