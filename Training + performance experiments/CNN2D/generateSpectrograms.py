from debugTools import showSpectrogram
import numpy as np
import os
import librosa
import cv2 as cv
import pandas as pd
from tqdm import tqdm
import shutil
from config import *

DEBUG = False

if DEBUG:
    import debugTools

# https://stackoverflow.com/questions/56719138/how-can-i-save-a-librosa-spectrogram-plot-as-a-specific-sized-image
def scale_minmax(X, min=0.0, max=1.0):
    specRange = X.max() - X.min()

    # Prevent divide by 0
    if specRange== 0:
        specRange=1

    X_std = (X - X.min()) / specRange
    X_scaled = X_std * (max - min) + min
    return X_scaled


def loadAudioFileAppended(filePath):
    # Load the audio
    result = librosa.load(filePath, sr=SR, mono=True)[0]

    # If the audio has not the correct length...
    if result.shape[0] != MIN_SAMPLES:
        # extend the audio clip by wraping the file
        result = librosa.util.fix_length(result, MIN_SAMPLES, mode="wrap")

    # return the result
    return result


def generateSpectrogram(audio):
    #compute melspectrogram
    melspec = librosa.feature.melspectrogram(
        audio,
        sr=SR,
        n_fft=N_FFT,
        hop_length= HOP_LENGTH,
        n_mels=N_MELS,
        fmin=0,
        fmax=SR // 2)

    return librosa.core.power_to_db(melspec)  #to convert a power spectrogram to dB units


def loadMetaData():
    filePath = os.path.join(US_FILE_PATH,"metadata/UrbanSound8K.csv")
    columns = ["slice_file_name", "fold", "classID"]
    return pd.read_csv(filePath, usecols=columns)


def saveSpectrogramToFile(filePath, spectrogram):
    # https://stackoverflow.com/questions/56719138/how-can-i-save-a-librosa-spectrogram-plot-as-a-specific-sized-image

    # min-max scale to fit inside 8-bit range
    img = scale_minmax(spectrogram, 0, 255).astype(np.uint8)
    img = np.flip(img, axis=0) # put low frequencies at the bottom in image

    # save as PNG
    cv.imwrite(filePath, img)


def createOutputFolders():
    # When debugging, don't delete the old files
    if DEBUG:
        return

    # If the output folder exists...
    if(os.path.isdir(SPEC_FILE_PATH)):
        # Delete the output folder and its content
        shutil.rmtree(SPEC_FILE_PATH)

    # Create the output folder
    os.mkdir(SPEC_FILE_PATH)

    # For each fold...
    for i in range(1,11):
        # Create the output folder for this fold
        os.mkdir(os.path.join(SPEC_FILE_PATH, f"fold{i}"))


def main():
    # Load the meta data
    metaData = loadMetaData()

    # Create the output folders 
    createOutputFolders()

    # Create an empty list to store the report information
    reportData = []

    # Go over each row in the pandas frame
    for _, row in tqdm(metaData.iterrows(), total=len(metaData)):
        # Generate the name of the fold
        foldFolderName = f"fold{row['fold']}"

        # Get the end part of the folder path
        fileName = row['slice_file_name']

        # Get the full file path to the wav file
        filePath = os.path.join(US_FILE_PATH, "audio", foldFolderName, fileName)

        # Load the audio of the wav file, use at least 3s of audio
        audio = loadAudioFileAppended(filePath)

        # Generate the spectrogram
        spectrogram = generateSpectrogram(audio)

        # When debugging
        if DEBUG:
            # Show the spectrogram
            debugTools.showSpectrogram(spectrogram, SR, fileName)

            # Stop
            return

        # Replace the file name extension
        fileName = fileName.replace(".wav", ".png")

        # Generate the output file path
        outputFilePath = os.path.join(SPEC_FILE_PATH, foldFolderName, fileName)

        # Add usefull information to the report
        reportData.append([fileName, outputFilePath, row['fold'], row['classID']])

        # Save the spectrogram
        saveSpectrogramToFile(outputFilePath, spectrogram)

    # Create a dataframe from the report data
    melSpec_df = pd.DataFrame(reportData, columns=["fileName", "filePath", "fold", "classID"])

    # Save the df as a pickle file
    melSpec_df.to_pickle(os.path.join(SPEC_FILE_PATH, SPEC_META_PKL))

    # Save the df as a csv file
    melSpec_df.to_csv(os.path.join(SPEC_FILE_PATH, SPEC_META_CSV))

    # Report that everything is finished
    print("Finished")

if __name__ == '__main__':
    # Run the main program
    main()