import os
# shut up TF
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys

import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow_model_optimization.quantization.keras import vitis_quantize

from config import *
from loadDataset import loadUrbansound8k

def quantize(conf: configuration):
    # Load the dataset, use all images EXCEPT the test fold for quantization
    (dataset, _), _, (x_test, y_test) = loadUrbansound8k(conf.testFold, conf.testFold)

    # Load the floating point trained model
    float_model = load_model(conf.keras_hdf5)

    # Run quantization
    quantizer = vitis_quantize.VitisQuantizer(float_model)
    quantized_model = quantizer.quantize_model(calib_dataset=dataset)

    # Save quantized model
    quantized_model.save(conf.quantizedModel)
    print('Saved quantized model to ', conf.quantizedModel)

    # Creating debug files
    #quantizer = vitis_quantize.VitisQuantizer.dump_model(quantized_model, x_test, "./Debug")


    '''
    Evaluate quantized model
    '''
    print('\n'+DIVIDER)
    print ('Evaluating quantized model..')
    print(DIVIDER+'\n')

    quantized_model.compile(optimizer=Adam(),
                            loss='categorical_crossentropy',
                            metrics=['accuracy'])

    scores = quantized_model.evaluate(x=x_test,y=y_test,batch_size=BATCH_SIZE, verbose=1)

    print('Quantized model accuracy (on test set): {0:.4f}'.format(scores[1]*100),'%')
    print('\n'+DIVIDER)


def main():
    # Load the configuration
    conf = configuration()

    # Print information for the log file
    print('\n'+DIVIDER)
    print('Keras version      : ',tf.keras.__version__)
    print('TensorFlow version : ',tf.__version__)
    print(sys.version)
    print(DIVIDER)
    conf.printQuantizationSettings()
    print(DIVIDER)

    # quantize the model
    quantize(conf)

if __name__ == '__main__':
    main()