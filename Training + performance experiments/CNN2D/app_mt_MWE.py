'''
Copyright 2020 Xilinx Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from ctypes import *
import cv2
import numpy as np
import runner
import os
from os.path import isdir, isfile, join
import xir
import vart
import threading
import time
import argparse

def getFolderNames(folderPath):
    # Get all folders inside the folder
    return [name for name in os.listdir(folderPath) if isdir(join(folderPath, name))]

def getFileNames(folderPath):
    return [name for name in os.listdir(folderPath) if isfile(join(folderPath, name))]


def getAllImageFilePaths(baseFolderPath):
    # Get the folder labels
    labels = getFolderNames(baseFolderPath)

    # Sort the labels
    labels.sort()

    # Create an empty list for the folder paths
    filePaths = []

    # For each group of images
    for i in range(len(labels)):
        # Get the path to the images folder
        folderPath = join(baseFolderPath, labels[i])

        # Get the image file names
        imageFileNames = getFileNames(folderPath)

        # Get the number of images
        numberOfImages = len(imageFileNames)

        # Allocate a list for the folder paths
        subList = [None]*numberOfImages

        # For each file...
        for i in range(numberOfImages):
            # Get the folder path
            imageFilePath = join(folderPath, imageFileNames[i])

            # Add the full path to the sublist
            subList[i] = imageFilePath

        # Extend the complete list with the sublist
        filePaths = filePaths + subList

    # Return the list of files
    return filePaths


def preprocess_fn(image_path):
    '''
    Image pre-processing.
    Rearranges from BGR to RGB then normalizes to range 0:1
    input arg: path of image file
    return: numpy array
    '''
    # open image as BGR
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    # normalize
    image = image/255.0

    # Return the image
    return image

def get_subgraph (g):
    sub = []
    root = g.get_root_subgraph()
    sub = [ s for s in root.toposort_child_subgraph()
            if s.has_attr("device") and s.get_attr("device").upper() == "DPU"]
    return sub 

def runDPU(dpu, images):
    '''get tensor'''
    inputTensors = dpu.get_input_tensors()
    outputTensors = dpu.get_output_tensors()
    # inputTensors -> List of all input layers
    # inputTensors[0] -> First (and in this case only) input layer
    # inputTensors[0].dims -> List of the dimensions of the input layer (1, 32, 32, 3)
    # tuple(inputTensors[0].dims) -> convert the list of input layers to a tuple of input layers
    shapeIn = tuple(inputTensors[0].dims)
    shapeOut = tuple(outputTensors[0].dims)

    batchSize = shapeIn[0]
    n_of_rows = len(images)
    count = 0
    write_index = 0
    while count < n_of_rows:
        if (count+batchSize<=n_of_rows):
            runSize = batchSize
        else:
            runSize=n_of_rows-count
        
        '''prepare batch input/output '''
        # [np.empty(shapeIn, dtype = np.float32, order = 'C')] -> Create a list containing ONE empty 4D array with dimensions: 1x32x32x3
        inputData = [np.empty(shapeIn, dtype = np.float32, order = 'C')]
        outputData = [np.empty(shapeOut, dtype = np.float32, order = 'C')]
        
        '''init input image to input buffer '''
        for j in range(runSize):
            # # Playing with pointers: imageRun will point to same object as inputData[0]
            # imageRun = inputData[0]

            # # shapeIn[1:] -> Tuple: (32, 32, 3) (= dimension of the input image)
            # # ==> Place the image in the input buffer
            # imageRun[j, ...] = img[(count+j)% n_of_images].reshape(shapeIn[1:])
            inputData[0][j, ...] = images[(count+j)% n_of_rows].reshape(shapeIn[1:])

        '''run with batch '''
        job_id = dpu.execute_async(inputData,outputData)
        dpu.wait(job_id)

        '''store output vectors '''
        for j in range(runSize):
            out_q[write_index] = outputData[0][j]
            write_index += 1
        count = count + runSize

def app(image_dir,model):

    ''' Load data '''
    print('Loading data')

    # Get the list of file paths
    imageList = getAllImageFilePaths(image_dir)

	# get the total amount of images to run
    runTotal = len(imageList)

    global out_q
    out_q = [None] * runTotal

    ''' Load the DPU assembly code '''
    g = xir.Graph.deserialize(model)
    subgraphs = get_subgraph(g)
    assert len(subgraphs) == 1 # only one DPU kernel

    dpu_runner = runner.Runner(subgraphs[0], "run")

    ''' preprocess images '''
    print('Pre-processing',runTotal,'images...')
    images = []
    for i in range(runTotal):
        images.append(preprocess_fn(imageList[i]))

    '''run threads '''
    #print('Starting',1,'threads...')
    dpu_thread = threading.Thread(target=runDPU, args=(dpu_runner, images))

    time1 = time.time()
    dpu_thread.start()
    dpu_thread.join()
    time2 = time.time()
    timetotal = time2 - time1

    fps = float(runTotal / timetotal)
    print("FPS=%.2f, total frames = %.0f , time=%.4f seconds" %(fps,runTotal, timetotal))


    ''' post-processing '''
    classes = ['air_conditioner','car_horn','children_playing','dog_bark','drilling','engine_idling','gun_shot','jackhammer','siren','street_music']  
    correct = 0
    wrong = 0
    print('output buffer length:',len(out_q))
    for i in range(len(out_q)):
        # Get the predicted class
        argmax = np.argmax((out_q[i]))
        predicted = classes[argmax]

        # Get the ground truth
        groundTruth = imageList[i].split('/')[-2]

        if (predicted==groundTruth):
            correct += 1
        else:
            wrong += 1
    accuracy = correct/len(out_q)
    print('Correct:',correct,'Wrong:',wrong,'Accuracy:', accuracy)


# only used if script is run as 'main' from command line
def main():

  # construct the argument parser and parse the arguments
  ap = argparse.ArgumentParser()  
  ap.add_argument('-d', '--image_dir',
                  type=str,
                  default="spectrograms",
                  help='Path to folder of images. Default is spectrograms')  
  ap.add_argument('-m', '--model',
                  type=str,
                  default='model/SBCNN.xmodel',
                  help='Path of .xmodel file.')

  args = ap.parse_args()  
  
  print ('Command line options:')
  print (' --image_dir : ', args.image_dir)
  print (' --model     : ', args.model)

  app(args.image_dir,args.model)

if __name__ == '__main__':
  main()

